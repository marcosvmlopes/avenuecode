Feature: US1_Create_Task


Scenario: creating Task step 1

#Navigating the menus and checking the menu My Tasks
    Given navigate with the presence of My Tasks in all options
    When application access
    And i click on the menus
    Then System should always keep visible the menu 'my tasks'

#Accessing the task list and checking the message.
    Given check the message in the task list
    When i enter the to-do list
    Then system should display the message 'Hey Marcos Vinicius Machado Lopes, this is your todo list for today:'

Scenario: creating Task step 2

#Creating tasks
    Given register a task
    When access to the list of tasks for a new registration
    And fill in the field with the name of the task and click in addtask
    Then system must register the task with the name entered
