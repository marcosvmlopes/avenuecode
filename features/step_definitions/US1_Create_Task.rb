# encoding: utf-8
#Navigating the menus and checking the menu My Tasks
#
#
Given(/^navigate with the presence of My Tasks in all options$/) do
    visit "/"
  end

When(/^application access$/) do
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul.nav.navbar-nav.navbar-right > li:nth-child(1) > a').click
    fill_in 'user_email', with: 'marcos.vmlopes@gmail.com'
    fill_in 'user_password', with: 's1a2b3a4'
    find(:css, '#new_user > input').click
  end

And(/^i click on the menus$/) do
    #click on ToDoApp
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-header > a').click
    assert_text('My Tasks')
    #click on Home
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul:nth-child(1) > li:nth-child(1) > a').click
    assert_text('My Tasks')
    #click on My Tasks
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul:nth-child(1) > li:nth-child(2) > a').click
    assert_text('My Tasks')
    #click on Welcome, Marcos Vinicius Machado Lopes!
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul.nav.navbar-nav.navbar-right > li:nth-child(1) > a').click
    assert_text('My Tasks')
  end

Then(/^System should always keep visible the menu 'my tasks'$/) do
    assert_text('My Tasks')
    puts 'Test has kept the My Tasks menu visible'
  end

#Accessing the task list and checking the message.
#
#
Given(/^check the message in the task list$/) do
    visit "/"
  end

When(/^i enter the to-do list$/) do
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul:nth-child(1) > li:nth-child(2) > a').click
  end
Then(/^system should display the message 'Hey Marcos Vinicius Machado Lopes, this is your todo list for today:'$/) do
    assert_text('Hey Marcos Vinicius Machado Lopes, this is your todo list for today:')
    puts 'Test ok'
  end

#Creating tasks
#
#
Given(/^register a task$/) do
    visit "/"
  end

When(/^access to the list of tasks for a new registration$/) do
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul.nav.navbar-nav.navbar-right > li:nth-child(1) > a').click
    fill_in 'user_email', with: 'marcos.vmlopes@gmail.com'
    fill_in 'user_password', with: 's1a2b3a4'
    find(:css, '#new_user > input').click
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul:nth-child(1) > li:nth-child(2) > a').click
  end

And(/^fill in the field with the name of the task and click in addtask$/) do
    fill_in 'new_task', with: 'testar app'
    find(:css, 'body > div.container > div.task_container.ng-scope > div.well > form > div.input-group > span').click
  end

Then(/^system must register the task with the name entered$/) do
    assert_text('testar app')
    puts 'create task ok'
  end
