# encoding: utf-8
#Creating a subtask
#
#
Given(/^creating a subtask$/) do
    visit "/"
  end

When(/^i access to the application and enter in subtask form$/) do
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul.nav.navbar-nav.navbar-right > li:nth-child(1) > a').click
    fill_in 'user_email', with: 'marcos.vmlopes@gmail.com'
    fill_in 'user_password', with: 's1a2b3a4'
    find(:css, '#new_user > input').click
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul:nth-child(1) > li:nth-child(2) > a').click
    find(:css, 'body > div.container > div.task_container.ng-scope > div.bs-example > div > table > tbody > tr > td:nth-child(4) > button').click
  end

And(/^i fill in the fields and click Save$/) do
    #clicar em ToDoApp
    fill_in 'edit_task', with: 'nova Task'
    fill_in 'new_sub_task', with: 'nova subTask'
    fill_in 'dueDate', with: '31/05/2017'
    find(:css, '#add-subtask').click
  end

Then(/^system must append the subTask to the list$/) do
    assert_text('Remove SubTask')
    puts 'Test add to subtask in list'
  end

#Delete tasks
#
#
Given(/^delete tasks$/) do
    visit "/"
  end

When(/^i access to the list of tasks for the delete$/) do
    find(:css, 'body > div.container > div.navbar.navbar-default > div > div.navbar-collapse.collapse > ul:nth-child(1) > li:nth-child(2) > a').click
  end

And(/^i select a task and click in remove$/) do
    find(:css, 'body > div.container > div.task_container.ng-scope > div.bs-example > div > table > tbody > tr > td:nth-child(5) > button').click
  end

Then(/^system must delete the selected task$/) do
    page.assert_no_selector('body > div.container > div.task_container.ng-scope > div.bs-example > div > table > tbody > tr > td.task_body.col-md-7 > a', :count => 2)
    puts 'delete task ok'
  end
